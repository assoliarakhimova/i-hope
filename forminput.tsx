import React, { ChangeEvent, HTMLInputTypeAttribute } from 'react';
import './form.css'

type Props = {
  label: string;
  name: string;
  type?: HTMLInputTypeAttribute;
  placeholder?: string;
  value: string;
  onChange: (v: ChangeEvent) => void;
  className?: string
  id?:string
  multiple?: boolean
};

export const FormInput: React.FC<Props> = ({
  label,
  name,
  type,
  onChange,
  value,
  placeholder,
  className,
  id,
  multiple
}) => {
  return (
    <div className="FormInput__formControl ">
      <label className ="FormInput__label">{label}</label>
      <input
        placeholder={placeholder}
        name={name}
        type={type}
        onChange={onChange}
        value={value}
        className = {className}
        id={id}
        multiple ={multiple}
      />
    </div>
  );
};
