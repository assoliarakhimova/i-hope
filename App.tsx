import React from 'react';
import './App.css';
import { PostForm, SignUpForm } from './eventform';
import Block from './block';

function App() {
  const p =(data:SignUpForm)=>{
    console.log(data)
  }
  return (
    <div className="App">
      <header className="App-header">
      <PostForm isLoading ={false} onSubmit ={p}/>
      </header>
    </div>
  );
}

export default App;
